<?php

/**
 * @param $uid
 *   The user's uid.
 * @param $stats
 *   The loaded statistics object.
 */
function hook_user_stats_alter($uid, $stats) {

}

/**
 * Invoked upon completion of recalculating user statistics.
 *
 * @param $uid
 *   The user's uid.
 * @param $stats
 *   The updated user stats.
 */
function hook_user_stats_updated($uid, $stats) {

}