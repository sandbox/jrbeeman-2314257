<?php

/**
 * @file
 * Entity index schema and index alterations for User stats module.
 */

/**
 * Implements hook_entity_index_fields().
 *
 * Join timestamp: Already provided in entity_index_user.
 *
 */
function user_stats_entity_index_extra_fields($entity_type) {
  $fields = array();

  if ($entity_type == 'user') {
    $fields['last_node'] = array(
      'description' => 'The last node NID posted by this user.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    );
    $fields['last_node_created'] = array(
      'description' => 'The timestamp of the last node posted by this user.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    );
    $fields['last_comment'] = array(
      'description' => 'The last comment CID posted by this user.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    );
    $fields['last_comment_created'] = array(
      'description' => 'The timestamp of the last comment posted by this user.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    );
    $fields['count_nodes'] = array(
      'description' => 'The total number of nodes posted by this user.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    );
    $fields['count_comments'] = array(
      'description' => 'The total number of comments posted by this user.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    );
  }

  return $fields;
}

/**
 * Implements hook_entity_index_post_populate_data().
 */
function user_stats_entity_index_post_populate_entity_user($entity_id) {
  $index = _user_stats_load_stats($entity_id);
  $index->count_nodes = _user_stats_node_count($entity_id);
  $index->count_comments = _user_stats_comment_count($entity_id);

  $node = _user_stats_last_node($entity_id);
  $index->last_node = $node->nid;
  $index->last_node_created = $node->created;

  $comment = _user_stats_last_comment($entity_id);
  $index->last_comment = $comment->cid;
  $index->last_comment_created = $comment->created;

  drupal_write_record('entity_index_user', $index, 'id');

  $stats = user_stats_load_stats($entity_id, TRUE);
  module_invoke_all('user_stats_updated', $entity_id, $stats);
}
